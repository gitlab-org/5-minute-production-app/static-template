# extract terraform state
gitlab-terraform output -json >tf_output.json
jq --raw-output ".private_key.value.private_key_pem" tf_output.json >private_key.pem
chmod 0600 private_key.pem

# variables
PUBLIC_IP=$(jq --raw-output ".public_ip.value" tf_output.json)
CERT_EMAIL=${CERT_EMAIL:-$TF_VAR_SERVICE_DESK_EMAIL}
CERT_DOMAIN=${CERT_DOMAIN:-$CI_COMMIT_REF_SLUG.$PUBLIC_IP.nip.io}
NGINX_CONF=$(cat conf.nginx)

# report dynamic_url
DYNAMIC_ENVIRONMENT_URL=https://$CERT_DOMAIN
echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >>deploy.env

# install nginx
# delete existing nginx conf (if exists)
# write nginx config
# create `public` dir
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i private_key.pem ubuntu@$PUBLIC_IP "
    sudo apt install nginx -y
    sudo nginx -v
    rm -f conf.nginx
    echo \"$NGINX_CONF\" >conf.nginx
    rm -rf public
    mkdir public
"

if [ $? -ne 0 ]; then
  exit 1
fi

# copy files
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i private_key.pem -r public/* ubuntu@$PUBLIC_IP:/home/ubuntu/public

if [ $? -ne 0 ]; then
  echo "🔴 Failed to copy 'public' directory"
  exit 1
fi

# kill running nginx process (if exists)
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i private_key.pem ubuntu@$PUBLIC_IP "
    sudo nginx -s stop && echo 'nginx: stopped'
"

if [ $? -ne 0 ]; then
  echo "nginx could not be stopped, but that's okay"
fi

# update package repos
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i private_key.pem ubuntu@$PUBLIC_IP "
    sudo snap refresh
    sudo snap install --classic certbot

    sudo certbot certonly                               \
        --non-interactive                               \
        --standalone                                    \
        --agree-tos                                     \
        --email $CERT_EMAIL                             \
        --domains $CERT_DOMAIN                          \
        --cert-name webapp_cert

    sudo chown ubuntu /etc/letsencrypt/live/webapp_cert/fullchain.pem
    sudo chown ubuntu /etc/letsencrypt/live/webapp_cert/privkey.pem
"

if [ $? -ne 0 ]; then
  exit 1
fi

# test nginx config
# start nginx process
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i private_key.pem ubuntu@$PUBLIC_IP '
    sudo nginx -t -c ~/conf.nginx
    sudo nginx -c ~/conf.nginx && echo "nginx: started"
'

if [ $? -ne 0 ]; then
  exit 1
fi
