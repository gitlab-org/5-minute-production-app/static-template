# Static Template

aka `One Minute Static`. 

This is a spin-off from the original [5 Minute Production](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template) concept. `5 Minute Production` makes it dead simple to set up production-grade infrastructure and GitOps workflows for webapps. 

Sometimes you're not building a webapp. Sometimes you just need to host and serve static content. 

Introducing `One Minute Static`. 

Create a GitLab repo, place your static content in the root-level `public` folder. Then watch `One Minute Static` deliver production grade infra and GitOps workflows for your static website.

## Usage

1. Create a `git` repo on GitLab (or on any other repo host with GitLab CICD enabled)
1. Create a root level `./public` folder and place your static content there
1. Create a root level `.gitlab-ci.yml` file with the following contents:
```yaml
include:
  remote: https://gitlab.com/gitlab-org/5-minute-production-app/static-template/-/raw/master/static.yml
```
Pipeline will now automatically provision, deploy, generate SSL certificates and serve `public` folder.

## Custom Domains

Setting up a custom domain is easy. Add relevant variables:
```yaml
variables:
  CERT_DOMAIN: my-custom-domain.com
  CERT_EMAIL: me@my-custom-domain.com

include:
  remote: https://gitlab.com/gitlab-org/5-minute-production-app/static-template/-/raw/master/static.yml
```

Setup DNS by adding `A` records and point it to the generated IP address.

## What It Does

1. Creates an AWS EC2 free-tier instance (`t2-micro`, Ubuntu 20.04)
1. Sets up Nginx and serves your `public` folder
1. Generates SSL certificates

## Examples

- https://gitlab.com/sri-at-gitlab/projects/unofficial-gitlab-tools
- https://gitlab.com/gitlab-org/5-minute-production-app/sandbox/cats
